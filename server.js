var express = require("express");
var getDateFromMes = require('./textComparation').getDateFromMes
var checkText = require('./textComparation').checkText
var checkName = require('./textComparation').checkName
var session = require('express-session')
var Cookies = require('cookie-parser')
var md5 = require('md5')
const pg = require('pg');
// Парсер JSON объектов в POST запросах. Получение с помощью req.body
var bodyParser = require('body-parser');
// Позволяет работать с окружением
require('dotenv').load();

var app = express();

// Описание сессии
app.use(session({
  secret: 'keyboard cat'
}))
// Для работы javascript файлов на клиентской стороне
app.use('/public', express.static('public'));
app.use(Cookies());
app.use(bodyParser.json());
var urlencodedParser = bodyParser.urlencoded({extended: false});
// Данные для подключения берутся из окружения для сохранноcти данных
const config = {
  host: process.env.HOST,
  user: process.env.DB_USERNAME,     
  password: process.env.PASSWORD,
  database: process.env.DBname,
  port: 5432,
  ssl: true
};
// Подключение клиента в базе данных
const client = new pg.Client(config);
client.connect(err => {
  if (err) {
    console.log(err)
  }
});
// Для работы шаблонизатора
app.set('view engine', 'ejs');

// Функция, проверяющая уровень доступа user - curators and teachers. SuperUser - only me
function loadUser(req, res, next) {
  console.log('check load user - ' + req.session.access)
  if (req.session.user_id){
    if (req.session.access=='teacher'||req.session.access=='curator') {
      // Запрос к бд на поиск человека в стафе
      next()
    } else {
      res.status(400).render('error.ejs', {
        error_code: '300',
        error_msg: 'You have no permissions. Sry('
      });
    }
  } else {
    res.redirect('/')
  }
  
  
}
function loadSuperUser(req, res, next){
  console.log('check super user' + req.session.user_id)
  if (req.session.user_id) {
    // Проверка супер юзера
    if(req.session.user_id=='32879395'){
      next()
    } else{
      res.status(400).render('error.ejs', {
        error_code: '300',
        error_msg: 'You have no permissions. Sry('
      });
    }
  } else {
    res.redirect('/');
  }
}


// Пост запрос для работы с VK Callback API. Ответ приходит в формате JSON
// {"type":"event", "object":{Описывается модель ответа}}
// в нашем случае модель ответа - message
// Подробнее об этом в https://vk.com/dev/callback_api
// Далее проверяем соответствует ли сообщению паттерну "проверено дата"
// в случае совпадения добавляем запись о посещении в бд
app.post('/callbackAPI', urlencodedParser, function(req, res){
  ans = req.body
  // Подтверждение сервера для группы
  if (ans.type=='confirmation'){
    if (ans.group_id=='169779433'){
      res.send('f49f48fd')
    } else{
      if (ans.group_id=='170184933'){
        res.send('1b1cea73')
      } else{
          if (ans.group_id=='169779466'){
            res.send('44161ac5')
          }
      }
    }
  }
  else {
    console.log('Got message')
    var mes = ans.object.text
    var date = Number(ans.object.date)
    var group_vk = ans.group_id
    var student_vk = ans.object.peer_id
    if (checkText(mes)){
      console.log('Text compare true')
      // Реализовать добавление элемента в таблицу посещения
      const doQuery = async () => {
        try {
          date = getDateFromMes(mes).split('.')
          month = date[1]
          day = date[0]
          year = '2018'
          start = new Date(Number(year), Number(month)-1, Number(day))
          console.log(month, day, year)
          console.log(start)
          await client.query('BEGIN')
          const res = await client.query('SELECT id FROM student_courses WHERE group_vk=$1 and student_vk=$2', [group_vk, student_vk])
          const insertAttendance = 'INSERT INTO attendance VALUES ($1, $2, $3) ON CONFLICT DO NOTHING'
          const insertAttendanceValues = [res.rows[0].id, true, start]
          
          await client.query(insertAttendance, insertAttendanceValues)
          await client.query('COMMIT')
          console.log('Added row to attendance')
        } catch (e) {
          await client.query('ROLLBACK')
          console.log('Smth wrong \n' + e)
          throw e
        }
      }
      doQuery()
    }
  }
  res.type('text')
  res.status(200).send('ok');
});

app.get('/attend',loadUser, function(req, res){
  if(req.query.student_vk){
    if (typeof(req.query.student_vk)!='string'){
      Array.from(req.query.student_vk).forEach((student_vk) => {
        const attend = async () => {
          try {
            date = req.query.date;
            group_vk = req.query.group_vk
            await client.query('BEGIN')
            const res = await client.query('SELECT id FROM student_courses WHERE group_vk=$1 and student_vk=$2', [group_vk, student_vk])
            const insertAttendance = 'INSERT INTO attendance VALUES ($1, $2, $3) ON CONFLICT DO NOTHING'
            const insertAttendanceValues = [res.rows[0].id, true, date]
            console.log(res)
            await client.query(insertAttendance, insertAttendanceValues)
            await client.query('COMMIT')
            console.log('Done adding student attendance')
          } catch (e) {
            await client.query('ROLLBACK')
            console.log(e);
            res.status(300).render('error.ejs', {
             error_code: '300',
             error_msg: e
            });
            throw e
          }
        }
        attend()
      })
    } else {
      const attend = async () => {
        try {
          date = req.query.date;
          group_vk = req.query.group_vk
          await client.query('BEGIN')
          const res = await client.query('SELECT id FROM student_courses WHERE group_vk=$1 and student_vk=$2', [group_vk, req.query.student_vk])
          const insertAttendance = 'INSERT INTO attendance VALUES ($1, $2, $3) ON CONFLICT DO NOTHING'
          const insertAttendanceValues = [res.rows[0].id, true, date]
          console.log(res)
          await client.query(insertAttendance, insertAttendanceValues)
          await client.query('COMMIT')
          console.log('Done')
        } catch (e) {
          await client.query('ROLLBACK')
          console.log(e);
          res.status(400).render('error.ejs', {
           error_code: '300',
           error_msg: e
          });
          throw e
        }
      }
      attend()
    }
  } else {
    res.status(300).render('error.ejs', {
      error_code: '300',
      error_msg: 'Undefined user, try again'
     });
  }
  
  res.status(200).redirect('/check_attendance');
})

// GET запросы для отображения страниц
app.get('/',function(req, res){
	res.render('start.ejs', {})
});

app.get('/check_attendance', loadUser, function(req, response){
  if (req.session.access=='curator'){
    const query = 'SELECT curator_group.group_vk, group_num FROM curator_group, groups WHERE curator_group.group_vk=groups.group_vk and curator_vk=$1;';  
    client.query(query, [req.session.user_id])
        .then(res => {
            const rows = res.rows;
            groups = []
            rows.map(row => {
                groups.push(row)
            });
            groups = JSON.stringify(groups)
            response.render('check_attendance.ejs',{
              groups: groups,
            });
        })
        .catch(err => {
            console.log(err);
            response.status(400).render('error.ejs', {
              error_code: '400',
              error_msg: err
            });
        });
  } else {
    if (req.session.access=='teacher'){
      const query = 'SELECT group_vk, group_num FROM groups WHERE course_id=$1;';  
      client.query(query, [req.session.course_id])
          .then(res => {
              const rows = res.rows;
              groups = []
              rows.map(row => {
                  groups.push(row)
              });
              groups = JSON.stringify(groups)
              response.render('check_attendance.ejs',{
                groups: groups,
              });
          })
          .catch(err => {
              console.log(err);
              response.status(400).render('error.ejs', {
                error_code: '300',
                error_msg: err
              });
          });
    } else{
      response.status(400).render('error.ejs', {
        error_code: '300',
        error_msg: 'You have no permissions. Sry(('
      });
    }
  }
});

// Возможно стоит сразу здесь редактировать куки, чтобы не отправлять на хоум параметры, из-за которых переход без них на страницу
// не получается. Но не выходит, если куки редактируются на сервере, то на клиенте они не видны
app.get('/home', function(req, res){
  var flag = false;
  if(req.query.hash==md5(process.env.APP_ID + String(req.query.uid) + process.env.SECRET_KEY)){ 
    req.session.user_id = req.query.uid
    var selectStaff = 'select curator_vk from curators'
    client.query(selectStaff)
        .then(response => {
              const rows = response.rows;
              staff = []
              rows.map(row => {
                staff.push(row)
              });
              staff.forEach(element => {
                  if (req.query.uid==element.curator_vk) {
                    console.log('curator is logged')
                    flag = true;
                    // req.session.access = 'curator';
                    req.session.access = 'curator'
                    res.status(200).render('home.ejs', {
                      uid: req.query.uid,
                      name: req.query.first_name,
                      photo_rec: req.query.photo_rec,
                      hash: req.query.hash
                    })
                  }
              });
            
            
        })
        .catch(err => {
            console.log(err);
            res.status(300).render('error.ejs', {
              error_code: '300',
              error_msg: err
            });
        });
    if (!flag){
      selectStaff = 'select teacher_vk, course_id from teachers'
      client.query(selectStaff)
          .then(response => {
            
              const rows = response.rows;
              staff = []
              rows.map(row => {
                staff.push(row)
              });
              staff.forEach(element => {
                  if (req.session.user_id==element.teacher_vk) {
                    console.log('teacher is logged')
                    req.session.access = 'teacher'
                    req.session.course_id = element.course_id
                    res.status(200).render('home.ejs', {
                      uid: req.query.uid,
                      name: req.query.first_name,
                      photo_rec: req.query.photo_rec,
                      hash: req.query.hash
                    })
                  }
              });
            
              
              
          })
          .catch(err => {
              console.log(err);
              res.status(300).render('error.ejs', {
                error_code: '300',
                error_msg: err
              });
          });
    } else {
      req.session.access = 'noone'
      res.status(200).render('home.ejs', {
        uid: req.query.uid,
        name: req.query.first_name,
        photo_rec: req.query.photo_rec,
        hash: req.query.hash
      })
    }
    
  }
  else{
    res.status(200).render('home.ejs', {
      uid: undefined,
      name: undefined,
      photo_rec: undefined,
      hash: undefined
    })
  }
});

app.get('/add_full_group', loadUser, function(req, response){
  const query = 'SELECT * FROM courses;';
    client.query(query)
        .then(res => {
            const rows = res.rows;
            courses = []
            rows.map(row => {
                courses.push(row)
            });
            courses = JSON.stringify(courses)
            response.render('add_full_group.ejs',{
              courses: courses,
            });
        })
        .catch(err => {
            console.log(err);
            res.status(300).render('error.ejs', {
              error_code: '300',
              error_msg: err
            });
        });
});

app.get('/db_work', loadSuperUser, function(req, response){
    const query = 'SELECT * FROM courses;';
    
    client.query(query)
        .then(res => {
            const rows = res.rows;
            courses = []
            rows.map(row => {
                courses.push(row)
            });
            courses = JSON.stringify(courses)
            response.status(200).render('db_work.ejs',{
              courses: courses,
            });
        })
        .catch(err => {
            console.log(err);
            res.status(300).render('error.ejs', {
              error_code: '300',
              error_msg: err
            });
        });
});

app.get('/help',function(req, res){
  res.render('help.ejs',{});
});

app.get('/courses',function(req, response){
  const query = 'SELECT * FROM courses;';  
    client.query(query)
        .then(res => {
            const rows = res.rows;
            courses = []
            rows.map(row => {
                courses.push(row)
            });
            courses = JSON.stringify(courses)
            response.render('courses.ejs',{
              courses: courses,
            });
        })
        .catch(err => {
            console.log(err);
            res.status(300).render('error.ejs', {
              error_code: '300',
              error_msg: err
            });
        });
});

app.get('/attendance', loadUser, function(req, response){
  group_vk = req.query.group_vk;
  // group_vk = 169779433;
  // date = new Date('2018-08-12')
  // if (getDateFromMes(req.query.date)){
  //   date = req.query.date;
  // } else{
  //   date = req.query.date + '.2018';
  // }
  date = getDateFromMes(req.query.date).split('.');
  const selectDumpStudents = 'select name, second_name, students.student_vk, student_courses.group_vk from students, student_courses\
  where students.student_vk not in\
    (select students.student_vk from students, student_courses, attendance\
    where students.student_vk=student_courses.student_vk\
    and student_courses.group_vk=$1\
    and student_courses.id=attendance.id\
    and attendance.date=$2) \
  and student_courses.group_vk=$1\
  and student_courses.student_vk=students.student_vk\
  order by second_name asc'
  const params = [group_vk, '2018-'+date[1]+'-'+date[0]]
  client.query(selectDumpStudents, params)
        .then(res => {
            const rows = res.rows;
            students = []
            rows.map(row => {
                students.push(row)
            });
            students = JSON.stringify(students)
            response.render('attendance.ejs',{
              students: students,
              date: '2018-'+date[1]+'-'+date[0]
            });
        })
        .catch(err => {
            console.log(err);
            res.status(300).render('error.ejs', {
              error_code: '300',
              error_msg: err
            });
        });
});
// Обработка запроса на добавление всех учеников из группы
// здесь транзакция на три запроса - добавление группы, добавление учеников, добавление ученикам курсов
// в случае существования строк - ничего не происходит
// последний запрос проверяет уникальность пары (group_id, student_id)
app.post('/full_students', loadUser, function(req,res){
  const doQuery = async () => {
    try {
      var student_vk = req.query.vk
      var name = req.query.name
      var second_name = req.query.second_name
      var group_vk = req.query.group_id
      var group_num = req.query.group_num
      var course_id = req.query.course_id
      await client.query('BEGIN')
      await client.query('INSERT INTO students VALUES($1, $2, $3) ON CONFLICT DO NOTHING', [student_vk, name, second_name])
      const insertGroup = 'INSERT INTO groups VALUES ($1, $2, $3) ON CONFLICT DO NOTHING'
      const insertGroupValues = [group_vk, group_num, course_id]
      await client.query(insertGroup, insertGroupValues)
      await client.query('INSERT INTO student_courses (student_vk, group_vk, paid) SELECT $1, $2, $3 WHERE NOT EXISTS (select distinct(student_vk, group_vk) from student_courses where student_vk=$1 and group_vk=$2)', [student_vk, group_vk, true])
      await client.query('COMMIT')
      res.send(200).send('Done')
    } catch (e) {
      await client.query('ROLLBACK')
      res.status(300).render('error.ejs', {
        error_code: '300',
        error_msg: err
      });
      throw e
    }
  }
  doQuery()
})

// Запрос формируется так: button.id(кнопка действия - добавить или удалить) + form.id(название таблицы)
// На данный момент добавление элементов в таблицы не сгруппированы
// Сделать можно так: сгруппировать по кол-ву параметров, в невидимые поля занести их название 
// и придумать единые название по типу first_arg = first_value и так далее
// Переделать все в пост запрос с возвращением success
// Обработка добавления элементов в таблицу учеников и учителя


app.get('/add_students', loadSuperUser, function(req, res){
  console.log(req.query)
  if (checkName(req.query.name)&&checkName(req.query.second_name)) {
    const query = 'INSERT INTO students VALUES ($1, $2, $3)';
    const params = [req.query.vk, req.query.name, req.query.second_name]
    client.query(query, params)
        .then(result => {
          res.status(200).redirect('/db_work')
        })
        .catch(err => {
          console.log(err)
          res.status(300).render('error.ejs', {
            error_code: '300',
            error_msg: err
          });
        });
  } else{
    res.status(300).render('error.ejs', {
      error_code: '300',
      error_msg: 'Please enter valid credentials'
    });
  }
  
});
app.get('/add_teachers', loadSuperUser, function(req, res){
  if (checkName(req.query.name)&&checkName(req.query.second_name)) {
    const query = 'INSERT INTO teachers VALUES ($1, $2, $3, $4)';
    const params = [req.query.vk, req.query.sub, req.query.name, req.query.second_name]
    client.query(query, params)
        .then(result => {
          res.redirect('/db_work')
        })
        .catch(err => {
          console.log(err)
          res.status(300).render('error.ejs', {
            error_code: '300',
            error_msg: err
          });
        });
  } else{
    res.status(300).render('error.ejs', {
      error_code: '300',
      error_msg: 'Please enter valid credentials'
    });
  }
    
});
// Обработка добавления элементов в таблицу ученик-курс 
app.get('/add_student_courses', loadSuperUser, function(req, res){
    const query = 'INSERT INTO student_courses (student_vk, group_vk, paid) SELECT $1, $2, $3 WHERE NOT EXISTS (select distinct(student_vk, group_vk) from student_courses where student_vk=$1 and group_vk=$2)';
    const params = [req.query.vk, req.query.group_vk, true]
    client.query(query, params)
        .then(result => {
          res.redirect('/db_work')
        })
        .catch(err => {
          console.log(err)
          res.status(300).render('error.ejs', {
            error_code: '300',
            error_msg: err
          });
        });
});
// Добавление группы
app.get('/add_groups', loadSuperUser, function(req, res){
    const query = 'INSERT INTO groups VALUES ($1, $2, $3)';
    const params = [req.query.vk, req.query.group_num, req.query.sub]
    client.query(query, params)
        .then(result => {
          res.redirect('/db_work')
        })
        .catch(err => {
          console.log(err)
          res.status(300).render('error.ejs', {
            error_code: '300',
            error_msg: err
          });
        });
});
// Добавление куратору группы
app.get('/add_curator_group', loadSuperUser, function(req, res){
    const query = 'INSERT INTO curator_group VALUES ($1, $2)';
    const params = [req.query.group_vk, req.query.curator_vk]
    client.query(query, params)
        .then(result => {
          res.redirect('/db_work')
        })
        .catch(err => {
          console.log(err)
          res.status(300).render('error.ejs', {
            error_code: '300',
            error_msg: err
          });
        });
});
// Добавление курса
app.get('/add_courses', loadSuperUser, function(req, res){
    if(!req.query.type=='full_year'){
      var bool = 'true'
    }
    else {
      var bool = 'false'
    }
    const query = 'INSERT INTO courses ("course_name", "price", "express", "description") VALUES ($1, $2, $3, $4)';
    const params = [req.query.course_name, req.query.price, bool, req.query.description]
    client.query(query, params)
        .then(result => {
          res.redirect('/db_work')
        })
        .catch(err => {
          console.log(err)
          res.status(300).render('error.ejs', {
            error_code: '300',
            error_msg: err
          });
        }); 
});
// Добавление куратора
app.get('/add_curators', loadSuperUser, function(req, res){
  if (checkName(req.query.name)&&checkName(req.query.second_name)) {
    const query = 'INSERT INTO curators VALUES ($1, $2, $3)';
    const params = [req.query.vk, req.query.name, req.query.second_name]
    client.query(query, params)
        .then(result => {
          res.redirect('/db_work')
        })
        .catch(err => {
          console.log(err)
          res.status(300).render('error.ejs', {
            error_code: '300',
            error_msg: err
          });
        });
  } else{
    res.status(300).render('error.ejs', {
      error_code: '300',
      error_msg: 'Please enter valid credentials'
    });
  }
    
});
// Удаление формируется так - есть два вида таблиц: с ключом айди вк и со своим ключом
// для первого типа достаточно определить айди вк и по нему производить удаление
// остальные (их две) по 2 значениям. Спецификация бд подразумеввают уникальность
// по этим двум значениям. primary_key хранятся как невидимые поля
// удаление
app.get('/delete', loadSuperUser, function(req, res){
    const query = 'DELETE FROM '+req.query.table_name+' WHERE '+req.query.primary_key+'=$1';
    const params = [req.query.vk]
    client.query(query, params)
        .then(result => {
          res.redirect('/db_work')
        })
        .catch(err => {
          console.log(err)
          res.status(300).render('error.ejs', {
            error_code: '300',
            error_msg: err
          });
        });
  
});
// Удаление курсов у учеников и групп у куратора
app.get('/delete_curator_group', loadSuperUser, function(req, res){
    const query = 'DELETE FROM curator_group WHERE '+req.query.primary_key+'=$1 and '+req.primary_key2+'=$2';
    const params = [req.query.group_vk, req.query.curator_vk]
    client.query(query,params)
        .then(result => {
          res.redirect('/db_work')
        })
        .catch(err => {
          console.log(err)
          res.status(300).render('error.ejs', {
            error_code: '300',
            error_msg: err
          });
        });
  
});
app.get('/delete_student_courses', loadSuperUser, function(req, res){
    const query = 'DELETE FROM student_courses WHERE '+req.query.primary_key+'=$1 and '+req.primary_key2+'=$2';
    const params = [req.query.student_vk, req.query.course_id]
    client.query(query)
        .then(result => {
          res.redirect('/db_work')
        })
        .catch(err => {
          console.log(err)
          res.status(300).render('error.ejs', {
            error_code: '300',
            error_msg: err
          });
        });
  
});

// Скорее всего просто убрать это. Есть возмоность удалить и добавить заново
// Изменение пока не буду делать

app.listen(process.env.PORT, function() {
  console.log('%s listening at %s', app.name, app.url);
});

// Старая версия - проверка даты, но выполнение излишнее, реализовать получение сообщений по клику невозможно
// Код жалко стирать, возможно пригодиться
// function checkAttendance(text, date, date_to_compare){
//   if (checkText(text)){
//     console.log('text contains needed substring')
//     if (!date_to_compare){
//       now = new Date()
//       year = now.getFullYear()
//       month = now.getMonth()
//       start_date = now.getDate()
//       start = new Date(year, month, start_date, 8)
//       end = new Date(year, month, start_date + 1, 8)
//     }
//     else {
//       year = date_to_compare.getFullYear()
//       month = date_to_compare.getMonth()
//       start_date = date_to_compare.getDate()
//       start = new Date(year, month, start_date, 8)
//       end = new Date(year, month, start_date + 1, 8)
//     }
//     console.log(start)
//     console.log(end)
//     console.log(date*1000)
//     now2 = new Date(date*1000)
//     console.log(now2)
//     if (now2>start && now2<end){
//       return true
//     }
//   }
//   return false
// }