describe("check the mes. Does it include the data", function() {
  var textComparation = require('../textComparation');
  
  it("should give the date", function(){
    date = textComparation.getDateFromMes('vdfkjvl12.12asdasd')
    expect(date).toEqual('12.12')
    date = textComparation.getDateFromMes('vdf9. 12')
    expect(date).toEqual(null)
    date = textComparation.getDateFromMes('vdfl   2.2    a  s dasd')
    expect(date).toEqual('2.2')
    date = textComparation.getDateFromMes('12:2asdasd')
    expect(date).toEqual(null)
    date = textComparation.getDateFromMes('vАбвдыжьа 2.10a йцуsdasd')
    expect(date).toEqual('2.10')
  })
  it("Does text compare to pattern", function(){
    expect(textComparation.checkText('ПРОВЕРЕНО')).toBeTruthy
    expect(textComparation.checkText('ПровЕренО')).toBeTruthy
    expect(textComparation.checkText('проверка')).toBeFalsy
  })
})
describe("check the attendance with curator", function() {
  var originalTimeout;
    beforeEach(function() {
      originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
      jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
    });
    afterEach(function() {
      jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });
  const pg = require('pg');
  var md5 = require('md5')
  let chai = require('chai');
  //let should = chai.should();
  let chaiHttp = require('chai-http');
  //let expect = chai.expect;
  chai.use(chaiHttp);
  require('dotenv').load();
  const config = {
    host: process.env.HOST,
    user: process.env.DB_USERNAME,     
    password: process.env.PASSWORD,
    database: process.env.DBname,
    port: 5432,
    ssl: true
  };
  const client = new pg.Client(config);
  client.connect(err => {
    if (err) {
      console.log('db')
      console.log(err)
    }
  });
  let base_url='https://sattarovwebservice.azurewebsites.net'
  let agent = chai.request(base_url)
  afterEach(function(done){
    client.query('DELETE FROM students WHERE student_vk=1').then((res)=>{
      done()
    })
    
  })
  it("register user", function(done){
      let url='/home'
      agent
        .get(url)
        .query({"uid":32879395, "first_name":"Дамир", "last_name":"Абдуллин", "photo_rec":"https://pp.userapi.com/c846120/v846120309/c458b/mhGJATv7mXc.jpg%3Fava=1", "hash": md5('6770005' + '32879395' + 'oVcmuphYZrbFNnkF3wJa')})
        .end(function(error, response) {
          expect(response.statusCode).toBe(200);
          done(); 
      }); 
  })

  it("go to work with data base", function(done){
    let url='/db_work'
    chai.request(base_url)
    .get(url)
    .end(function(error, response) {
      expect(response.status).toEqual(200);
      done();
    
  });
  })

  it("Add student", function(done){
    let url='/add_students'
    agent
    .get(url)
    .query({"vk": 1, "name": "Бог", "second_name": "Олимпа"})
    .set('Cookie', 'connect.sid='+process.env.CONNECT_SID)
    .end(function(error, response) {
      expect(response.statusCode).toBe(200);
      client.query('SELECT * FROM students WHERE student_vk=1').then(res =>{
        let rows = res.rows;
        expect(rows.length).toBe(1);
        expect(rows[0].student_vk).toBe(1);
        expect(rows[0].name).toBe("Бог");
        expect(rows[0].second_name).toBe("Олимпа");
        // следующий тест 
        done();
      })
    
    });
  })

  it("Add student_courses", function(done){
    let url='/add_students'
    agent
    .get(url)
    .query({"vk": 1, "name": "Бог", "second_name": "Олимпа"})
    .set('Cookie', 'connect.sid='+process.env.CONNECT_SID)
    .end(function(error, response) {
      expect(response.statusCode).toBe(200);
      client.query('SELECT * FROM students WHERE student_vk=1').then(res =>{
        let rows = res.rows;
        expect(rows.length).toBe(1);
        expect(rows[0].student_vk).toBe(1);
        expect(rows[0].name).toBe("Бог");
        expect(rows[0].second_name).toBe("Олимпа");
        // следующий тест 
        let url='/add_student_courses'
        agent
        .get(url)
        .query({"vk": 1, "group_vk": 169779466})
        .set('Cookie', 'connect.sid='+process.env.CONNECT_SID)
        .end(function(error, response) {
          expect(response.statusCode).toBe(200);
          client.query('SELECT * FROM student_courses WHERE student_vk=1 and group_vk=169779466').then(res =>{
            let rows = res.rows;
            expect(rows.length).toBe(1);
            expect(rows[0].student_vk).toBe(1);
            expect(rows[0].group_vk).toBe(169779466);
            // следующий тест 
            done();
          })
      })
    
    });
    
    
    });
  })

  it("Check the POST method, that works with CallbackAPI positive", function(done){
    // 8 декабря от человек 284047589
    let url='/add_students'
    agent
    .get(url)
    .query({"vk": 1, "name": "Бог", "second_name": "Олимпа"})
    .set('Cookie', 'connect.sid='+process.env.CONNECT_SID)
    .end(function(error, response) {
      expect(response.statusCode).toBe(200);
      client.query('SELECT * FROM students WHERE student_vk=1').then(res =>{
        let rows = res.rows;
        expect(rows.length).toBe(1);
        expect(rows[0].student_vk).toBe(1);
        expect(rows[0].name).toBe("Бог");
        expect(rows[0].second_name).toBe("Олимпа");
        // следующий тест 
        let url='/add_student_courses'
        agent
        .get(url)
        .query({"vk": 1, "group_vk": 169779466})
        .set('Cookie', 'connect.sid='+process.env.CONNECT_SID)
        .end(function(error, response) {
          expect(response.statusCode).toBe(200);
          client.query('SELECT * FROM student_courses WHERE student_vk=1 and group_vk=169779466').then(res =>{
            let rows = res.rows;
            expect(rows.length).toBe(1);
            expect(rows[0].student_vk).toBe(1);
            expect(rows[0].group_vk).toBe(169779466);
            // следующий тест 
            let url = '/callbackAPI'
            agent
            .post(url)
            .send({"type":"message_reply","object":{"date":1544347903,"from_id":32879395,"id":25412,"out":1,"peer_id":284047589,"text":"Проверено 08.12","conversation_message_id":475,"fwd_messages":[],"important":false,"random_id":0,"attachments":[],"admin_author_id":32879395,"is_hidden":false},"group_id":169779433})
            .end(function(error, response) {
              expect(response.statusCode).toBe(200);
              expect(response.text).toEqual('ok')
              client.query('SELECT * FROM students WHERE student_vk=1').then(res =>{
                let rows = res.rows;
                expect(rows.length).toBe(1);
                expect(rows[0].student_vk).toBe(1);
                expect(rows[0].name).toBe("Бог");
                expect(rows[0].second_name).toBe("Олимпа");
                done();
              })
            });
          })
      })
    
    });
    
    
    });

    
  })
})

// apachejemeter??
// BrowserStack
// Jasmine Stand Alone для клиентской части
// jasmine js - framework itself
// jasmine html 
// boot.js - starts tests

//next add the source files to test

//next - include all spec files (the tests' logic)