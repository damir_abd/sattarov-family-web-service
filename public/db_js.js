var back = document.getElementById('back')
var forms = document.getElementsByClassName('form') 
// forms - формы для заполнения данных, используется для того, чтобы сделать ненужные невидимыми
var btns = document.getElementsByClassName('choose')
// btns - кнопки выбора формы для заполнения, используется для определения формы для отображения
var action = document.getElementById('action')
// action - кнопки действий: Изменить, Добавить, Удалить

start_html()
// пофиксить бы, задать изначально дисплей none, но не горит
// невидимы все формы и кнопки выбора, видимы кнопки действий
function start_html(){
    Array.from(forms).forEach((item) =>{
        item.style.display = 'None'
    })
    choose.style.display = 'None'
    back.style.display = 'None'
    action.style.display = 'block'
}
// Обработка нажатия кнопки НАЗАД, возвращаем просто в первоначальные вид страницу
back.addEventListener('click', () =>{
    start_html()
}) 
// Обработчик куки. Формат входных данных - строка вида "name=Value; second_name=Value"
// Здесь с помощью регулярного выражения возвращается значение name из cookie
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
// Обработчик множества кнопок класса choose, который убирает все формы, кроме нужной form_ + btn.id
var current_form = forms.item(1)
document.querySelectorAll("button.choose").forEach((button) => {
    button.addEventListener('click', () => {
        choose.style.display = 'None'
        Array.from(forms).forEach((item) => {
            if (item.id=='form_'+button.id) {
                item.style.display = 'block'
                // Здесь проверяется значение url из куки под названием action. Например значение /delete
                // Получено оно из обработчика нажатия выбора действия с бд. Далее просто формируется дальнейший 
                // вид URL относительно формы, в которой мы находимся и передается в action атрибут формы
                // чтобы она отправила запрос на добавление
                if (getCookie('action').localeCompare('/delete')==0){
                    if (button.id=="course_student" || button.id=="curator_group"){
                        item.setAttribute('action', getCookie('action') + '_' + button.id)
                    }
                    else {
                        item.setAttribute('action', getCookie('action'))
                    }
                }
                else {
                    item.setAttribute('action', getCookie('action') + '_' + button.id)
                }
                current_form = item
            }
        })
        back.style.display = 'block'
    });
});
// Обработка выбора действия: Удалить или Добавить
// Добавляется в куки формат URL, чтобы сделать обращение к серверу на нужный запрос изменения бд
document.querySelectorAll("button.action").forEach((button) => {
    button.addEventListener('click', () => {
        action.style.display = 'None'
        choose.style.display = 'block'
        Array.from(forms).forEach((item) => {
            document.cookie = 'action=' + '/' + button.id +"; path=/;"
        })
    });
});

      // Валидация форм
      // needs-validation-person - валидация, где присутствует только поле с айди vk, где указывается
      // id человека
      // needs-validation-group - аналогично, только с группами. id имеет формат group_vk
      // needs-validation-person-group - и то, и то. Обязательно соблюдение формата id.
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms_user = document.getElementsByClassName('needs-validation-person');
        var forms_groups = document.getElementsByClassName('needs-validation-group');
        var forms_user_groups = document.getElementsByClassName('needs-validation-person-group');
        // Loop over them and prevent submission
        var validation_users = Array.prototype.filter.call(forms_user, function(form) {
                form.addEventListener('submit', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    console.log(form.elements['vk'].value)
                    // прописать это, если все херово
                    VK.api('users.get', {"user_ids":form.elements['vk'].value, "v":"5.73"}, function(ans){
                        if(ans.error){
                            form.elements['vk'].setCustomValidity(ans.error.error_msg)
                        }
                        else{
                            form.submit()
                        }
                        
                    })
                    // это в конце
                    // form.classList.add('was-validated');
                }, false);
        });
        var validation_groups = Array.prototype.filter.call(forms_groups, function(form) {
                form.addEventListener('submit', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    console.log(form.elements['vk'].value)
                    // прописать это, если все херово
                    VK.api('groups.getById', {"group_id":form.elements['group_vk'].value, "v":"5.73"}, function(ans){
                        if(ans.error){
                            form.elements['group_vk'].setCustomValidity(ans.error.error_msg)
                        }
                        else{
                            form.submit()
                        }
                        
                    })
                    // это в конце
                    // form.classList.add('was-validated');
                }, false);
        });
        var validation_user_groups = Array.prototype.filter.call(forms_user_groups, function(form) {
                form.addEventListener('submit', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    console.log(form.elements['vk'].value)
                    // прописать это, если все херово
                    VK.api('groups.getById', {"group_id":form.elements['group_vk'].value, "v":"5.73"}, function(ans){
                        if(ans.error){
                            form.elements['group_vk'].setCustomValidity(ans.error.error_msg)
                        }
                        else{
                            VK.api('users.get', {"user_ids":form.elements['vk'].value, "v":"5.73"}, function(ans){
                                if(ans.error){
                                    form.elements['vk'].setCustomValidity(ans.error.error_msg)
                                }
                                else{
                                    form.submit()
                                }
                                
                            })
                        }
                        
                    })
                    // это в конце
                    // form.classList.add('was-validated');
                }, false);
        });
    }, false);
})();
    

// Получение данных из форм, устаревший формат, когда выбор был после выбор формы, теперь наоборот
// document.querySelectorAll("button.action").forEach((button) => {
//     console.log(button.id)
//     button.addEventListener('click', () => {
//         var values = []
//         var data = {}
//         if (button.id == 'insert') {
//             //insert values
//             current_form.querySelectorAll("input.form-control").forEach((item) =>{ //все input с
//                 // здесь. Text  input.value
//                 data[item.id] = item.value
//             })
//             if (current_form.id=='form_work_with_student' || current_form.id=='form_work_with_course_student'
//                 || current_form.id=='form_work_with_group' || current_form=='form_work_with_curator') {
//                     course_name.querySelectorAll("input.custom-control-input").forEach((item) =>{ //все checkboxes с
//                         // здесь.checkboxes checkboxe.checked 
//                         data[item.id] = item.checked
//                     })
//                 }
            
//             console.log(data)
//         }
//         else {
//             if (button.id == 'update') {
//                 //update values
//             }
//             else {
//                 //delete values
//             }
//         }
//     });
// });