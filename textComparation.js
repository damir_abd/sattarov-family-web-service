exports.getDateFromMes = function(text){
    reg = /(0[1-9]|[12][0-9]|3[01]|[1-9])\.(0[1-9]|1[012]|[1-9])/g
    //reg = /[([0-2]\d)(3[0-1])(\d)]\.[(0[1-9])[1-9](1[0-2])]/
    if (text.match(reg)){
      return (text.match(reg)[0])
    }
    return null
  }
  // Проверка текста сообщения на наличие ключа
  exports.checkText = function(text){
    if (~text.search(/проверено/i)){
      // В строке есть подстрока олодец (не учитываю регистр)
      return true
    }
    return false
  }
  exports.checkName = function(text){
    if (~text.search(/[А-Я][а-я]+/)){
      // В строке есть подстрока олодец (не учитываю регистр)
      return text.match(/[А-Я][а-я]+/)[0]
    }
    return text.match(/[А-Я][а-я]+/)
  }